// js-HW7 - filter-array

// ## Теоретичні питання

// 1. Опишіть своїми словами як працює метод forEach.

//    Цей метод перебирає кожен елемент масиву та виконує функцію,
//    яку передано йому, для кожного елемента.



// 2. Як очистити масив?

//  За допомогою присвоєння - присвоїти пустий масив або через функцию конструктор
//   let arr = [1, 2, 3];
//   arr = [];

//   або через функцию конструктор
//   arr = Array();

//  Інший варіант через зміну довжіні масиву
//   arr.length = 0



// 3. Як можна перевірити, що та чи інша змінна є масивом?

//    arr.isArray() - якщо так то поверне true, якщр ні то false



// ## Завдання

// Реалізувати функцію фільтру масиву за вказаним типом даних.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Написати функцію `filterBy()`, яка прийматиме 2 аргументи.
//   Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані,
//   які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом.
//   Тобто якщо передати масив ['hello', 'world', 23, '23', null],
//   і другим аргументом передати 'string', то функція поверне масив [23, null].



function filterBy(arrayAnyData, dataType) {
  const newArray = [];

  for (let anyElement of arrayAnyData) {
    if(typeof anyElement !== dataType)
    {
      if(anyElement !== null || dataType !== "null")
        newArray.push(anyElement);
    }
  }
  return newArray;
}

console.log(filterBy(["hello", "world", 24, "23", null], "string"));

console.log(filterBy([[], {}, 0, 23, "24", null, true, undefined], "number"));

console.log(filterBy(["hello", [1], {i:1}, 0, null, 25, "23", ""], "null"));

console.log(filterBy([undefined, "world", 24, "23", null, false], "undefined"));

console.log(filterBy([undefined, "world", 22, true, null], "number"));

console.log(filterBy(["hello", 1, 0, false, 23, true, null], "boolean"));
